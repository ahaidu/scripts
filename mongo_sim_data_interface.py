# -*- coding: utf-8 -*-
"""
  Software License Agreement (BSD License)

  Copyright (c) 2015, Andrei Haidu, Institute for Artificial Intelligence,
  Universität Bremen.
  All rights reserved.

  @author: Andrei Haidu
"""

from pymongo import MongoClient
import numpy as np


class MngSimDataIface:
    
    ###########################################################################
    # init class with the mongo host, db name and coll name
    def __init__(self, host_name, db_name, coll_name):
        self.conn = MongoClient(host_name, 27017)
        self.db = self.conn[db_name]
        self.coll = self.db[coll_name]



    ###########################################################################        
    # get the POSE and timestamp of the model at the most recent time    
    def get_model_pose_at(self, ts, model_name):
        # query for the most recent position using aggregation
        query_res = self.coll.aggregate([
                { "$match" : {"$and" : [{"timestamp" : { "$lte" : ts}}, {"models.name" : model_name}]}},
                { "$sort" : { "timestamp" : -1 }},
                { "$limit" : 1 },
                
                { "$unwind" : "$models"},    
                { "$match" : {"models.name" : model_name}},

                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.pos",
                    "rot" : "$models.rot"
                    }},                
            ])
            
        # get the result first(only) value
        #query_res = query_res['result'][0]
        query_res = list(query_res)[0]   
        
        # return pose (POS XYZ, ROT XYZ, timestamp)
        return np.array([query_res['pos']['x'], query_res['pos']['y'], query_res['pos']['z'],
                query_res['rot']['x'], query_res['rot']['y'], query_res['rot']['z'],
                query_res['timestamp']])



    ###########################################################################
    # get the POSE and timestamp of the link at the most recent time    
    def get_link_pose_at(self, ts, model_name, link_name):
        # query for the most recent position using aggregation
        query_res = self.coll.aggregate([
                { "$match" : {"$and" : [{"timestamp" : { "$lte" : ts}}, {"models.name" : model_name}]}},
                { "$sort" : { "timestamp" : -1 }},
                { "$limit" : 1 },
                
                { "$unwind" : "$models"},    
                { "$match" : {"models.name" : model_name}},
                { "$project" : {"models" : { "links" : 1 } , "timestamp" : 1, "_id" : 0}},
    
                { "$unwind" : "$models.links"},
                { "$match" : {"models.links.name" : link_name}},

                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.links.pos",
                    "rot" : "$models.links.rot"
                    }},                
            ])
            
        # get the result first(only) value
        #query_res = query_res['result'][0]
        query_res = list(query_res)[0]   
        
        # return pose (POS XYZ, ROT XYZ, timestamp)
        return np.array([query_res['pos']['x'], query_res['pos']['y'], query_res['pos']['z'],
                query_res['rot']['x'], query_res['rot']['y'], query_res['rot']['z'],
                query_res['timestamp']])



    ###########################################################################
    # get the POSE and timestamp of the collision at the most recent time    
    def get_collision_pose_at(self, ts, model_name, link_name, collision_name):
        # query for the most recent position using aggregation
        query_res = self.coll.aggregate([
                { "$match" : {"$and" : [{"timestamp" : { "$lte" : ts}}, {"models.name" : model_name}]}},
                { "$sort" : { "timestamp" : -1 }},
                { "$limit" : 1 },
                
                { "$unwind" : "$models"},    
                { "$match" : {"models.name" : model_name}},
                { "$project" : {"models" : { "links" : 1 } , "timestamp" : 1, "_id" : 0}},
    
                { "$unwind" : "$models.links"},
                { "$match" : {"models.links.name" : link_name}},
                { "$project" : {"models.links.collisions" : 1  , "timestamp" : 1}},
                
                { "$unwind" : "$models.links.collisions"},
                { "$match" : {"models.links.collisions.name" : collision_name}},
                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.links.collisions.pos",
                    "rot" : "$models.links.collisions.rot"
                    }},                
            ])
            
        # get the result first(only) value
        #query_res = query_res['result'][0]
        query_res = list(query_res)[0]   
        
        # return pose (POS XYZ, ROT XYZ, timestamp)
        return np.array([query_res['pos']['x'], query_res['pos']['y'], query_res['pos']['z'],
                query_res['rot']['x'], query_res['rot']['y'], query_res['rot']['z'],
                query_res['timestamp']])
                
                
                
    ###########################################################################
    # get the TRAJECTORY of the model between the timestamps
    def get_model_traj(self, ts_start, ts_end, model_name):
        # query between the two timestamps using aggregation
        query_res = self.coll.aggregate([
                { "$match" : {"timestamp" : { "$gt" : ts_start, "$lt" : ts_end}}},
                
                { "$unwind" : "$models"},   
                { "$match" : {"models.name" : model_name}},

                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.pos",
                    "rot" : "$models.rot"
                    }}   
            ])
            
        # get the results from the query
        #query_res = query_res['result']
        query_res = list(query_res)
        
        # if there are results in the given timestamp return them, otherwise get the most recent pose
        if len(query_res) > 0:
            # init trajectory as numpy array
            traj = np.zeros((len(query_res),7))
            
            # numpy array iter
            i = 0   
        
            # add every resulting step to the trajectory (POS XYZ, ROT XYZ, timestamp)
            for query_step in query_res:            
                traj[i] = ([query_step['pos']['x'], query_step['pos']['y'], query_step['pos']['z'],
                             query_step['rot']['x'], query_step['rot']['y'], query_step['rot']['z'],
                             query_step['timestamp']])
                # inc i
                i += 1
            
            # numpy array of POS XYZ ROT XYZ timestamp
            return traj     
            
        # return the most recent pose    
        else:
            print ' *** No position found between the given timestamp, returning the most recent pose'   
            return self.get_model_pose_at(ts_start, model_name)       
        
        
        
    ###########################################################################
    # get the TRAJECTORY of the link between the timestamps
    def get_link_traj(self, ts_start, ts_end, model_name, link_name):
        # query between the two timestamps using aggregation
        query_res = self.coll.aggregate([
                { "$match" : {"timestamp" : { "$gt" : ts_start, "$lt" : ts_end}}},
                
                { "$unwind" : "$models"},   
                { "$match" : {"models.name" : model_name}},
                { "$project" : {"models" : { "links" : 1 } , "timestamp" : 1, "_id" : 0}},
                
                { "$unwind" : "$models.links"},
                { "$match" : {"models.links.name" : link_name}},

                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.links.pos",
                    "rot" : "$models.links.rot"
                    }}   
            ])
            
        # get the results from the query
        #query_res = query_res['result']
        query_res = list(query_res)
        
        # if there are results in the given timestamp return them, otherwise get the most recent pose
        if len(query_res) > 0:
            # init trajectory as numpy array
            traj = np.zeros((len(query_res),7))
            
            # numpy array iter
            i = 0   
        
            # add every resulting step to the trajectory (POS XYZ, ROT XYZ, timestamp)
            for query_step in query_res:            
                traj[i] = ([query_step['pos']['x'], query_step['pos']['y'], query_step['pos']['z'],
                             query_step['rot']['x'], query_step['rot']['y'], query_step['rot']['z'],
                             query_step['timestamp']])
                # inc i
                i += 1
            
            # numpy array of POS XYZ ROT XYZ timestamp
            return traj     
        # return the most recent pose    
        else:
            print ' *** No position found between the given timestamp, returning the most recent pose'   
            return [self.get_model_pose_at(ts_start, model_name)]
        
        
        
    ###########################################################################
    # get the TRAJECTORY of the collision between the timestamps
    def get_collision_traj(self, ts_start, ts_end, model_name, link_name, collision_name):
        # query between the two timestamps using aggregation
        query_res = self.coll.aggregate([
                { "$match" : {"timestamp" : { "$gt" : ts_start, "$lt" : ts_end}}},
                
                { "$unwind" : "$models"},   
                { "$match" : {"models.name" : model_name}},
                { "$project" : {"models" : { "links" : 1 } , "timestamp" : 1, "_id" : 0}},
                
                { "$unwind" : "$models.links"},
                { "$match" : {"models.links.name" : link_name}},
                { "$project" : {"models.links.collisions" : 1  , "timestamp" : 1}},
                
                { "$unwind" : "$models.links.collisions"},
                { "$match" : {"models.links.collisions.name" : collision_name}},
                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.links.collisions.pos",
                    "rot" : "$models.links.collisions.rot"
                    }}   
            ])   
            
        # get the results from the query
        #query_res = query_res['result']
        query_res = list(query_res)
        
        # if there are results in the given timestamp return them, otherwise get the most recent pose
        if len(query_res) > 0:
            # init trajectory as numpy array
            traj = np.zeros((len(query_res),7))
            
            # numpy array iter
            i = 0   
        
            # add every resulting step to the trajectory (POS XYZ, ROT XYZ, timestamp)
            for query_step in query_res:
                traj[i] = ([query_step['pos']['x'], query_step['pos']['y'], query_step['pos']['z'],
                             query_step['rot']['x'], query_step['rot']['y'], query_step['rot']['z'],
                             query_step['timestamp']])
                # inc i
                i += 1
            
            # numpy array of POS XYZ ROT XYZ timestamp
            return traj     
        # return the most recent pose    
        else:
            print ' *** No position found between the given timestamp, returning the most recent pose'   
            return [self.get_model_pose_at(ts_start, model_name)]
        

