# -*- coding: utf-8 -*-
"""
  Software License Agreement (BSD License)

  Copyright (c) 2015, Andrei Haidu, Institute for Artificial Intelligence,
  Universität Bremen.
  All rights reserved.

  @author: Andrei Haidu
"""
from pymongo import MongoClient
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import proj3d




###########################################################################        
# get the roundness of the pancake and the pose numpy array
def get_roundness_and_poses(db, coll_name):

    # find document with metadata, and the positions of the links
    cursor = db[coll_name].find({'metadata' : {'$exists' : 'true'}}, {'metadata' : 1, 'links_pos' :1 })
    
    # get the document
    doc = cursor.next()
    
    # get the roundness value
    roundness = doc['metadata']['roundness']

    # get the links positions
    links_pos = doc['links_pos']
    
    # init numpy array with the length of the links pos result, and the XYZ feat
    poses_arr = np.zeros((len(links_pos),3))

    # numpy array iter
    i = 0   
    
    # add the results to the numpy array (XYZ)
    for pos in links_pos:                 
        poses_arr[i] = ([pos['x'], pos['y'], pos['z']])
        # increment the iter
        i += 1
    
    # return the roundness and the poses array
    return roundness, poses_arr


###########################################################################        
# get the pouring traj
def get_pouring_traj(db, coll_name):
    # query for all the results (there should be only one document)
    cursor = db[coll_name].find()
        
    # init trajectory as numpy array
    traj = np.zeros((cursor.count(),7))   
    
    # numpy array iter
    i = 0        
    
    # iterate through results and add them to the trajectory
    for c in cursor:
        # add value to numpy array
        traj[i] = [c['pos']['x'], c['pos']['y'], c['pos']['z'],
                     c['rot']['x'], c['rot']['y'], c['rot']['z'],
                     c['timestamp']]
        # inc i
        i += 1
        
    # return traj in the form [[POS XYZ, ROT XYZ, timestamp], .. ]
    return traj 
    

###########################################################################        
# plot the pancake  
def plot_pancake(roundness, poses_arr):

    plt.figure()
    plt.plot(poses_arr[:,0], poses_arr[:,1], 'o',\
        markersize=10, color='red', alpha=0.8, label='pancake')   
    
    plt.title('Pancake, Roundness: '+str(roundness))


###########################################################################        
# plot the pouring scenario pancake  
def plot_scenario3d(roundness, pos_arr, traj_arr):

    fig = plt.figure()

    ax = fig.add_subplot(111, projection='3d')
    # plot pouring
    ax.plot(traj_arr[:,0], traj_arr[:,1], traj_arr[:,2], 'o',\
        markersize=6, color='blue', alpha=0.8, label='pouring')
                
    # plot the starting point
    ax.plot([traj_arr[0][0]], [traj_arr[0][1]], [traj_arr[0][2]], '^',\
        markersize=10, color='green', alpha=0.8, label='pouring start')

    # end index
    i_end = len(traj_arr) - 1
    # plot the end point
    ax.plot([traj_arr[i_end][0]], [traj_arr[i_end][1]], [traj_arr[i_end][2]], 's',\
        markersize=10, color='magenta', alpha=0.8, label='pouring end')
        
    # plot the pancake
    ax.plot(pos_arr[:,0], pos_arr[:,1], pos_arr[:,2], 'o',\
        markersize=10, color='red', alpha=0.8, label='pancake')
        
    # set the labels
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    
    plt.title('Pouring, Pancake roundness: '+str(roundness))
    plt.legend(loc=3,prop={'size':8})
    

###########################################################################        
# plot the pouring scenario pancake  
def add_to_shared_plot(xt, yt, zt, traj_arr, color):    
           
   
    xt.plot(traj_arr[:,6], traj_arr[:,0], color)
    xt.set_ylabel('x [m]')
    

    yt.plot(traj_arr[:,6], traj_arr[:,1], color)
    yt.set_ylabel('y [m]')
    
  
    zt.plot(traj_arr[:,6], traj_arr[:,2], color)
    zt.set_xlabel('ts [s]')
    zt.set_ylabel('z [m]')
    
    
    
    
    xticklabels = xt.get_xticklabels() + yt.get_xticklabels()
    setp(xticklabels, visible=False)
    

###########################################################################        
# main
if __name__ == '__main__':

    # connect to mongo    
    conn = MongoClient('localhost', 27017)
    
    # connect to the database
    db = conn['roundness_pancake_pos']
    
    db_pouring = conn['roundness_pouring_traj']
    
    # get the all the collections
    coll_names = db.collection_names()
    
    share_fig = plt.figure()
    xt = share_fig.add_subplot(311) 
    yt = share_fig.add_subplot(312, sharex=xt, sharey=xt)    
    zt = share_fig.add_subplot(313, sharex=xt, sharey=xt) 
    
    
    # iterate through all the collections from the db
    for coll_name in coll_names:        
        # make sure the collection name differs from the "system.indexes"
        if coll_name != "system.indexes":
                
            # get the roundness and the poses array
            (roundness, poses_arr) = get_roundness_and_poses(db, coll_name)
            pouring_traj = get_pouring_traj(db_pouring, coll_name)
           
            # make trajectory to start from 0
            norm_traj = pouring_traj - pouring_traj[0]
            
            if (roundness < 0.3 and roundness > 0):                
                print coll_name, ' roundness: ' , roundness            
                # plot the pancake and the roundness
                #plot_scenario3d(roundness, poses_arr, pouring_traj)                
                add_to_shared_plot(xt, yt, zt, norm_traj, 'cyan')

            elif (roundness < 0.7 and roundness > 0.3):
#                print coll_name, ' roundness: ' , roundness            
#                # plot the pancake and the roundness
#                plot_scenario3d(roundness, poses_arr, pouring_traj)
                add_to_shared_plot(xt, yt, zt, norm_traj, 'red')
                
            elif (roundness < 1 and roundness > 0.7):
#                print coll_name, ' roundness: ' , roundness            
#                # plot the pancake and the roundness
#                plot_scenario3d(roundness, poses_arr, pouring_traj)
                add_to_shared_plot(xt, yt, zt, norm_traj, 'blue')
