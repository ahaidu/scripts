# -*- coding: utf-8 -*-
"""
  Software License Agreement (BSD License)

  Copyright (c) 2015, Andrei Haidu, Institute for Artificial Intelligence,
  Universität Bremen.
  All rights reserved.

  @author: Andrei Haidu
"""

from mongo_sim_data_interface import MngSimDataIface
from matplotlib import pyplot as plt
import csv
import os


###############################################################################
# example of quering the raw database and visualising the results
def query_and_vis_example():
    # connect to mongo via the interface class
    m1 = MngSimDataIface('localhost', 'SIM_pf_T', 'pf_T_1_raw')    
    
    # get pancake maker pose when grasping the bottle
    pancake_maker_pose = m1.get_model_pose_at(5.859, 'PancakeMaker')    
    # get the pancake center pose when grasping the spatula
    pancake_center_pose = m1.get_link_pose_at(24.956, 'LiquidTangibleThing', 'sphere_link_4')    
    # get the bottle top traj during grasp
    bottle_top_traj = m1.get_collision_traj(5.859, 21.983, 'Mondamin', 'mondamin_link', 'mondamin_event_collision')    
    # get the spatula head traj during grasp
    spatula_head_traj = m1.get_collision_traj(24.956, 33.263, 'Spatula', 'spatula_head_link', 'spatula_head_collision')
    
         
    ### Plot example
    fig1 = plt.figure(figsize=(20,20))
    
    ### SUBPLOT 1, positions
    sb1 = fig1.add_subplot(111, projection='3d')    
    # plot position of the pancake
    sb1.plot([pancake_center_pose[0]], [pancake_center_pose[1]], [pancake_center_pose[2]], '^',\
        markersize=10, color='blue', alpha=0.8, label='pancake center')    
    # plot position of the pancake maker
    sb1.plot([pancake_maker_pose[0]], [pancake_maker_pose[1]], [pancake_maker_pose[2]], '^',\
        markersize=10, color='red', alpha=0.8, label='pancake maker center')    
    # plot positions of the bottle top
    sb1.plot(bottle_top_traj[:,0], bottle_top_traj[:,1], bottle_top_traj[:,2], 'o',\
        markersize=7, color='orange', alpha=0.5, label='bottle top traj') 
    # plot positions of the spatula head
    sb1.plot(spatula_head_traj[:,0], spatula_head_traj[:,1], spatula_head_traj[:,2], 'o',\
        markersize=7, color='magenta', alpha=0.5, label='spatula head traj')   
    # label
    plt.title('SIM pour flip traj')
    plt.legend(loc=3,prop={'size':10})




###############################################################################
# example of looping through all the collections of the given db and querying them
def loop_through_all_collections_example():
    
    # !!! Make sure you load the right pancake center file for the given db !!!
    pancake_center_file = "pf_T_pancake_center.txt"    
    
    # create mongo sim object, with an emtpy string for the collection name
    mng_interface = MngSimDataIface('localhost', 'SIM_pf_T', " ") 
    
    # mongo collection name prefix
    mng_coll_prefix = "pf_T_"  
    
    # get all collection names
    coll_names = mng_interface.db.collection_names()
    
    # loop through all the collections
    for coll_name in coll_names:
        
        # make sure the collection name differs from the "system.indexes"
        if coll_name != "system.indexes":
                   
            # split the name of the collection
            coll_splits = coll_name.split("_")
    
            # get the last part of the split which shows the type of the coll
            coll_type = coll_splits[len(coll_splits) - 1]         
                   
            # get the index of the collection
            coll_index = coll_splits[len(coll_splits) - 2]
            
            # if the collection is of type event get the needed times and process the raw data
            if coll_type == "ev":
                
                print "======================================="
                print "--- Processing experiment nr: ", coll_index, "---"
    
                # get event coll to read needed event times
                ev_coll = mng_interface.db[coll_name]
    
                # set raw collection from which to query the poses          
                mng_interface.coll = mng_interface.db[mng_coll_prefix + coll_index + "_raw"]
                
                
                
                ##################################################################
                ### Get the bottle top traj from the moment of grasp until the release
                cursor = ev_coll.find({"event" : "GraspMondamin"})
                
                # check that the query has only one result, meaning only one grasping event on the bottle
                if(cursor.count() > 1):
                    print " *** Multiple results at GraspMondamin, from coll: ", coll_name, \
                    " !!! LOOK INTO IT MANUALLY !!!! <--------------------------------------------"
                else:
                    # get the first result of the cursor
                    c = cursor.next()
                
                    # get start and end time of the event
                    start_ts = c["start"]
                    end_ts = c["end"]
                    
                    # get the trajectory of the bottle top
                    bottle_top_traj = mng_interface.get_collision_traj(start_ts, end_ts, 'Mondamin', 'mondamin_link', 'mondamin_event_collision')
                    print 'Bottle top traj array: ', bottle_top_traj
    
    
    
                ##################################################################
                ### Get the spatula head center traj from the moment of grasp until the release
                cursor = ev_coll.find({"event" : "GraspSpatula"})
                
                # check that the query has only one result, meaning only one grasping event on the bottle
                if(cursor.count() > 1):
                    print " *** Multiple results at GraspSpatula, from coll: ", coll_name, \
                    " !!! LOOK INTO IT MANUALLY !!!! <--------------------------------------------"
                else:
                    # get the first result of the cursor
                    c = cursor.next()
                
                    # get start and end time of the event
                    start_ts = c["start"]
                    end_ts = c["end"]
                    
                    # get the trajectory of the spatula head link
                    spatula_head_traj = mng_interface.get_link_traj(start_ts, end_ts, 'Spatula', 'spatula_head_link')
                    print 'Spatula head traj array: ', spatula_head_traj
                    
                    
                
                ##################################################################
                ### Get the center of the pancake maker
                ### !!! CAREFUL !!! it's the center of mass, not the top plane center as in the robot data case
                # the pancake maker is static so it's not that crucial which timestamp you take
                cursor = ev_coll.find({"event" : "GraspMondamin"})
                
                # get the first result of the cursor
                c = cursor.next()
                
                # get start time of the event
                start_ts = c["start"]
                    
                # get the pose of the pancake maker
                pancake_maker_pose = mng_interface.get_model_pose_at(start_ts, 'PancakeMaker')
                print 'Pancake maker center pose array: ', pancake_maker_pose
                
                
                
                ##################################################################
                ### Get the center of the pancake when then spatula is grasped by reading it from the file
                ### since the center is different for every experiment
                cursor = ev_coll.find({"event" : "GraspSpatula"})
                
                # get the first result of the cursor
                c = cursor.next()
                
                # get start time of the event
                start_ts = c["start"]
                
                # !!! Make sure the right pancake center file is loaded !!!
                # open the pancake center list text file
                fp = open(pancake_center_file)
                
                # read only the line from the file which is equal to the collection index
                for i, line in enumerate(fp):              
                    # check that the line nr equals the index                    
                    if i == int(coll_index):
                        # the center link name is the first split from the line
                        center_link = line.split(",")[0]
                
                # close file
                fp.close()
                    
                # get the pose of the pancake maker
                pancake_center_pose = mng_interface.get_link_pose_at(start_ts, 'LiquidTangibleThing', center_link)
                print 'Pancake center pose array: ', pancake_center_pose




###############################################################################
# example of looping through all the collections of the given db, querying them and writing to csv files
def write_csv_files_example():
    
    # !!! Make sure you load the right pancake center file for the given db !!!
    #pancake_center_file_path = "pancake_centers/push_F_pancake_center.txt"    
    
    # create mongo sim object, with an emtpy string for the collection name
    mng_interface = MngSimDataIface('localhost', 'SIM_pf_pour_F', " ") 
    
    # mongo collection name prefix
    mng_coll_prefix = "pf_pour_F_"    
    
    # path to save the csv files
    csv_data_path = 'csv_data/SIM_pf_pour_F/'  
    
    # get all collection names
    coll_names = mng_interface.db.collection_names()
    
    # loop through all the collections
    for coll_name in coll_names:
        
        # make sure the collection name differs from the "system.indexes"
        if coll_name != "system.indexes":
                   
            # split the name of the collection
            coll_splits = coll_name.split("_")
    
            # get the last part of the split which shows the type of the coll
            coll_type = coll_splits[len(coll_splits) - 1]         
                   
            # get the index of the collection
            coll_index = coll_splits[len(coll_splits) - 2]
            
            # if the collection is of type event get the needed times and process the raw data
            if coll_type == "ev":
                
                print "======================================="
                print "--- Processing experiment nr: ", coll_index, "---"
    
                # get event coll to read needed event times
                ev_coll = mng_interface.db[coll_name]
    
                # set raw collection from which to query the poses          
                mng_interface.coll = mng_interface.db[mng_coll_prefix + coll_index + "_raw"]
                
                
                
                ##################################################################
                ### Get the bottle top traj from the moment of grasp until the release
                cursor = ev_coll.find({"event" : "GraspMondamin"})
                
                # check that the query has only one result, meaning only one grasping event on the bottle
                if(cursor.count() > 1):
                    print " *** Multiple results at GraspMondamin, from coll: ", coll_name, \
                    " !!! LOOK INTO IT MANUALLY !!!! <--------------------------------------------"
                else:
                    # get the first result of the cursor
                    c = cursor.next()
                
                    # get start and end time of the event
                    start_ts = c["start"]
                    end_ts = c["end"]
                                        
                    # get the trajectory of the bottle top
                    bottle_top_traj = mng_interface.get_collision_traj(start_ts, end_ts, 'Mondamin', 'mondamin_link', 'mondamin_event_collision')
                    print 'Writing bottle top traj array to ', csv_data_path + mng_coll_prefix + coll_index + "/bottle_top_traj_" + coll_index + ".csv"
                                        
                    # create folder if it doesn't exist
                    if not os.path.exists(csv_data_path + mng_coll_prefix + coll_index):
                        os.makedirs(csv_data_path + mng_coll_prefix + coll_index)
                    
                    # open csv file for writing
                    with open(csv_data_path + mng_coll_prefix + coll_index + "/bottle_top_traj_" + coll_index + ".csv", 'w') as csv_file:
                        traj_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                        
                        # add the trajectory to the file line by line
                        for traj_step in bottle_top_traj:
                            traj_writer.writerow(traj_step)
                    
                    
    
    
                """
                ##################################################################
                ### Get the spatula head center traj from the moment of grasp until the release
                cursor = ev_coll.find({"event" : "GraspSpatula"})
                
                # check that the query has only one result, meaning only one grasping event on the bottle
                if(cursor.count() > 1):
                    print " *** Multiple results at GraspSpatula, from coll: ", coll_name, \
                    " !!! LOOK INTO IT MANUALLY !!!! <--------------------------------------------"
                else:
                    # get the first result of the cursor
                    c = cursor.next()
                
                    # get start and end time of the event
                    start_ts = c["start"]
                    end_ts = c["end"]
                    
                    # get the trajectory of the spatula head link
                    spatula_head_traj = mng_interface.get_link_traj(start_ts, end_ts, 'Spatula', 'spatula_head_link')
                    print 'Writing spatula head traj array to ', csv_data_path + mng_coll_prefix + coll_index + "/spatula_head_traj_" + coll_index + ".csv"
                    
                    # create folder if it doesn't exist
                    if not os.path.exists(csv_data_path + mng_coll_prefix + coll_index):
                        os.makedirs(csv_data_path + mng_coll_prefix + coll_index)
                    
                    # open csv file for writing
                    with open(csv_data_path + mng_coll_prefix + coll_index + "/spatula_head_traj_" + coll_index + ".csv", 'w') as csv_file:
                        traj_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                        
                        # add the trajectory to the file line by line
                        for traj_step in spatula_head_traj:
                            traj_writer.writerow(traj_step)
                    
                """    
                
                ##################################################################
                ### Get the center of the pancake maker
                ### !!! CAREFUL !!! it's the center of mass, not the top plane center as in the robot data case
                # the pancake maker is static so it's not that crucial which timestamp you take
                cursor = ev_coll.find({"event" : "GraspMondamin"})
                
                # get the first result of the cursor
                c = cursor.next()
                
                # get start time of the event
                start_ts = c["start"]
                    
                # get the pose of the pancake maker
                pancake_maker_pose = mng_interface.get_model_pose_at(start_ts, 'PancakeMaker')
                print 'Writing pancake maker center pose array to ', csv_data_path + mng_coll_prefix + coll_index + "/pancake_maker_center_" + coll_index + ".csv"
                
                # create folder if it doesn't exist
                if not os.path.exists(csv_data_path + mng_coll_prefix + coll_index):
                    os.makedirs(csv_data_path + mng_coll_prefix + coll_index)
                
                # open csv file for writing
                with open(csv_data_path + mng_coll_prefix + coll_index + "/pancake_maker_center_" + coll_index + ".csv", 'w') as csv_file:
                    traj_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    
                    # add the trajectory to the file 
                    traj_writer.writerow(pancake_maker_pose)
                
                
                """
                ##################################################################
                ### Get the center of the pancake when then spatula is grasped by reading it from the file
                ### since the center is different for every experiment
                cursor = ev_coll.find({"event" : "GraspSpatula"})
                
                # get the first result of the cursor
                c = cursor.next()
                
                # get start time of the event
                start_ts = c["start"]
                
                # !!! Make sure the right pancake center file is loaded !!!
                # open the pancake center list text file
                fp = open(pancake_center_file_path)
                
                # read only the line from the file which is equal to the collection index
                for i, line in enumerate(fp):              
                    # check that the line nr equals the index                    
                    if i == int(coll_index):
                        # the center link name is the first split from the line
                        center_link = line.split(",")[0]
                
                # close file
                fp.close()
                    
                # get the pose of the pancake maker
                pancake_center_pose = mng_interface.get_link_pose_at(start_ts, 'LiquidTangibleThing', center_link)
                print 'Writing pancake center pose array to ', csv_data_path + mng_coll_prefix + coll_index + "/pancake_center_" + coll_index + ".csv"  
                
                # create folder if it doesn't exist
                if not os.path.exists(csv_data_path + mng_coll_prefix + coll_index):
                    os.makedirs(csv_data_path + mng_coll_prefix + coll_index)
                
                # open csv file for writing
                with open(csv_data_path + mng_coll_prefix + coll_index + "/pancake_center_" + coll_index + ".csv", 'w') as csv_file:
                    traj_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    
                    # add the trajectory to the file 
                    traj_writer.writerow(pancake_center_pose)
                """


###############################################################################
# example of quering the raw database and visualising the results
def chem_query_and_vis_example():
    # connect to mongo via the interface class
    m1 = MngSimDataIface('localhost', 'SIM-pancake-db', 'coll-pancake100_raw')    
    
    # get pancake maker pose when grasping the bottle
    pancake_maker_pose = m1.get_model_pose_at(3.36, 'PancakeMaker')    
    # get the pancake center pose when grasping the spatula
    pancake_center_pose = m1.get_link_pose_at(12.756, 'LiquidTangibleThing', 'sphere_link_4')    
    # get the bottle top traj during grasp
    bottle_top_traj = m1.get_collision_traj(3.36, 10.1, 'Mondamin', 'mondamin_link', 'mondamin_event_collision')    
    # get the spatula head traj during grasp
    spatula_head_traj = m1.get_collision_traj(12.7, 18.230, 'Spatula', 'spatula_head_link', 'spatula_head_collision')
    
         
    ### Plot example
    fig1 = plt.figure(figsize=(20,20))
    
    ### SUBPLOT 1, positions
    sb1 = fig1.add_subplot(111, projection='3d')    
    # plot position of the pancake
    sb1.plot([pancake_center_pose[0]], [pancake_center_pose[1]], [pancake_center_pose[2]], '^',\
        markersize=10, color='blue', alpha=0.8, label='pancake center')    
    # plot position of the pancake maker
    sb1.plot([pancake_maker_pose[0]], [pancake_maker_pose[1]], [pancake_maker_pose[2]], '^',\
        markersize=10, color='red', alpha=0.8, label='pancake maker center')    
    # plot positions of the bottle top
    sb1.plot(bottle_top_traj[:,0], bottle_top_traj[:,1], bottle_top_traj[:,2], 'o',\
        markersize=7, color='orange', alpha=0.5, label='bottle top traj') 
    # plot positions of the spatula head
    sb1.plot(spatula_head_traj[:,0], spatula_head_traj[:,1], spatula_head_traj[:,2], 'o',\
        markersize=7, color='magenta', alpha=0.5, label='spatula head traj')   
    # label
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('SIM pour flip traj')
    plt.legend(loc=3,prop={'size':10})




### Usage examples
if __name__ == '__main__':


    ###########################################################################
    # query and visualization example
    #query_and_vis_example()
 
 
    ###########################################################################
    # loop and query example
    #loop_through_all_collections_example()


    ###########################################################################
    # write csv files
    #write_csv_files_example()
    
    