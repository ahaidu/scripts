#!/usr/bin/env python  
import rospy
import tf
import time
from pymongo import MongoClient
from sensor_msgs.msg import Joy
import threading
import sys

btn_pressed = False
log_state = 0
pancake_pos = []
pancake_maker_pos = []


def joy_listener():
    # subscribe to the joystick topic
    rospy.Subscriber("joy", Joy, on_msg)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
    

def on_msg(joy_data):
    
    #global variables
    global btn_pressed
    global log_state
    
    # button pressed
    if(joy_data.buttons[11] == 1):    
        btn_pressed = True
    
    # button released
    if(btn_pressed and joy_data.buttons[11] == 0):
        btn_pressed = False        
        
        if(log_state == 0):
            print "** Saving static poses! \n"
            log_state += 1

        elif(log_state == 1):
            print "** Starting logger! \n"
            log_state += 1
        else:
            print "** Stoping logger! \n"
            log_state += 1


if __name__ == '__main__':
    
    ex_index = sys.argv[1]    
    
    # refernce to the global variable
    global log_state  
    
    # declare the static frames flag
    static_frames_saved = False
    
    # init ros node
    rospy.init_node('tf_pr2_pour_flip')      
    
    # create mongo client
    client = MongoClient('localhost', 27017)
    
    # database name
    db = client['robot_flip_push_off_F']     
    
    # the fixed frame (TODO map or base link?)
    fixed_frame = '/odom_combined'
    
    # frames to be logged
    # frames: /r_spatula_blade
    # pancake_bottle
    record_frames = ['/r_spatula_blade']
   
    # flag for mongo meta data
    frame_meta_flags = {}
    
    # mongo collection names
    frame_db_colls = {}
    
    # create the mongo meta flags and the collection names
    for frame in record_frames:        
        frame_meta_flags[frame] = False
        frame_db_colls[frame] = db[frame + '_' + ex_index]
     
    # print them out
    for frame in record_frames:
        print frame_meta_flags[frame]
        print frame_db_colls[frame]
    
    # save the current starting time in milliseconds
    start_time = int(round(time.time() * 1000))
    
    # create an start a thread for listening on the joy topic
    joy_th = threading.Thread(target=joy_listener)
    joy_th.start()

    # create tf transf listener
    tf_listener = tf.TransformListener()
    
    # loop with the given freq
    rate = rospy.Rate(10.0)
    
    while not rospy.is_shutdown():
        try: 
            # logging sthe static values
            if (log_state == 1): 
                
                # save static frames only once
                if (not static_frames_saved):                
                    # get time
                    ts_ms = int(round(time.time() * 1000)) - start_time
                    print "\t Saving static points at ", ts_ms
                    
                    #cluster0_frame - pancake maker
                    (pm_trans, pm_rot) = tf_listener.lookupTransform(fixed_frame, '/cluster0_frame', rospy.Time(0))
    
                    pm_euler = tf.transformations.euler_from_quaternion(pm_rot)                
                    
                    pm_meta_post = {"metadata":
                                        {"type": "trajectory",
                                         "description": "Pancake maker static pose"},
                                     "timestamp": ts_ms,
                                     "pos":
                                         { "x" : pm_trans[0],
                                           "y" : pm_trans[1],
                                           "z" : pm_trans[2] + 0.02}, # the vision is offseted with ~2cm on z
                                     "rot": 
                                         { "x" : pm_euler[0],
                                           "y" : pm_euler[1],
                                           "z" : pm_euler[2]}
                                }
                                                     
                    db['pancake_maker_' + ex_index].insert(pm_meta_post)
                    
                    
                    #cluster1_frame - pancake
                    (p_trans, p_rot) = tf_listener.lookupTransform(fixed_frame, '/cluster1_frame', rospy.Time(0))
    
                    p_euler = tf.transformations.euler_from_quaternion(p_rot)                
                    
                    p_meta_post = {"metadata":
                                        {"type": "trajectory",
                                         "description": "Pancake static pose"},
                                     "timestamp": ts_ms,
                                     "pos":
                                         { "x" : p_trans[0],
                                           "y" : p_trans[1],
                                           "z" : p_trans[2]},
                                     "rot": 
                                         { "x" : p_euler[0],
                                           "y" : p_euler[1],
                                           "z" : p_euler[2]}
                                }
                                                     
                    db['pancake_' + ex_index].insert(p_meta_post)
                    
                    
                    # set static frame flag to true
                    static_frames_saved = True           
                
            
            # log the given tf frames
            elif(log_state == 2):                
                ts_ms = int(round(time.time() * 1000)) - start_time
                print "\t Saving tf at ", ts_ms
                
                for frame in record_frames:
                    
                    (trans,rot) = tf_listener.lookupTransform(fixed_frame, frame, rospy.Time(0))
    
                    euler = tf.transformations.euler_from_quaternion(rot)
    
                    print frame, ": ", trans
    
                    if not frame_meta_flags[frame]:
                        meta_post = {"metadata":
                                        {"type": "trajectory",
                                         "description": "Pr2 pour flip trajectory of TOOL NR"},
                                     "timestamp": ts_ms,
                                     "pos":
                                         { "x" : trans[0],
                                           "y" : trans[1],
                                           "z" : trans[2]},
                                     "rot": 
                                         { "x" : euler[0],
                                           "y" : euler[1],
                                           "z" : euler[2]}
                                    }
                                     
                        frame_meta_flags[frame] = True
                        
                        frame_db_colls[frame].insert(meta_post)
                        
                    else:
                        post = {"timestamp": ts_ms,
                                     "pos":
                                         { "x" : trans[0],
                                           "y" : trans[1],
                                           "z" : trans[2]},
                                     "rot": 
                                         { "x" : euler[0],
                                           "y" : euler[1],
                                           "z" : euler[2]}
                                } 
                        frame_db_colls[frame].insert(post)
                
            #else:
            #    print "not logging.."

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        
        rate.sleep()
