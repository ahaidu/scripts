# -*- coding: utf-8 -*-
"""
Scripts for editing google charts in html files.
"""
import os
import fileinput

# user values
dir = "./"
filename = ""
filename_key = "zh"
comment_keys = ["Contact", "Closed"]
keep_keys = ["Closed"]

# filename
if not filename:
    for file in os.listdir(dir):
        if file.endswith(".html") and file.startswith("Timeline"):
            if not filename_key:
                filename = file
                break
            elif filename_key in file:
                filename = file
                break
print(" ** selected filename: %s" % filename)


# path
in_path = dir + filename
out_path = dir + "O_" + filename
print(" ** path to file: %s" % in_path)


# comment out requested lines
def should_comment(rstr):
    for key in comment_keys:                
        if key in rstr:
            return True
    return False

def should_keep(rstr):
    for key in keep_keys:                
        if key in rstr:
            return True
    return False

with open(in_path) as infile:
   with open(out_path, 'w') as outfile:
      for line in infile:
         rstr = line.rstrip()
         if rstr.endswith('],') and not should_keep(rstr) and should_comment(rstr):
             outfile.write('//' + line)
         else:
             outfile.write(line)
        