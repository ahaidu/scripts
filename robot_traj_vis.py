# -*- coding: utf-8 -*-
"""
  Software License Agreement (BSD License)

  Copyright (c) 2015, Andrei Haidu, Institute for Artificial Intelligence,
  Universität Bremen.
  All rights reserved.

  @author: Andrei Haidu
  
"""

from pymongo import MongoClient
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import proj3d



### class for getting the flip scenarios
class RobotFlipTraj:
    
    ###########################################################################
    # init class with the mongo host, db name and pancake, pancake maker and spatula blade coll names
    def __init__(self, host_name, db_name, p_coll_name, pm_coll_name, sp_coll_name):
        self.conn = MongoClient(host_name, 27017)
        self.db = self.conn[db_name]
        self.p_coll = self.db[p_coll_name]    
        self.pm_coll = self.db[pm_coll_name]
        self.sp_coll = self.db[sp_coll_name]   
        


    ###########################################################################
    # get the center pose of the pancake surface
    def get_pancake_pose(self):
        # query for all the results (there should be only one document)
        cursor = self.p_coll.find()
            
        # get the result first(only) value
        res = cursor.next()
        
        # return pose (POS XYZ, ROT XYZ, timestamp)
        return np.array([res['pos']['x'], res['pos']['y'], res['pos']['z'],
                res['rot']['x'], res['rot']['y'], res['rot']['z'],
                res['timestamp']])
        
        
        
    ###########################################################################
    # get the center pose of the pancake maker surface
    def get_pancake_maker_pose(self):
        # query for all the results (there should be only one document)
        cursor = self.pm_coll.find()
            
        # get the result first(only) value
        res = cursor.next()
        
        # return pose (POS XYZ, ROT XYZ, timestamp)
        return np.array([res['pos']['x'], res['pos']['y'], res['pos']['z'],
                res['rot']['x'], res['rot']['y'], res['rot']['z'],
                res['timestamp']])
                
                
                
    ###########################################################################
    # get the center pose of the pancake maker surface
    def get_spatula_traj(self):
        
        # query for all the results (there should be only one document)
        cursor = self.sp_coll.find()    
        
        # init trajectory as numpy array
        traj = np.zeros((cursor.count(),7))   
        
        # numpy array iter
        i = 0        
        
        # iterate through results and add them to the trajectory
        for c in cursor:
            # add value to numpy array
            traj[i] = [c['pos']['x'], c['pos']['y'], c['pos']['z'],
                         c['rot']['x'], c['rot']['y'], c['rot']['z'],
                         c['timestamp']]
            # inc i
            i += 1
            
        # return traj in the form [[POS XYZ, ROT XYZ, timestamp], .. ]
        return traj        
                



### class for getting the pour scenarios
class RobotPourTraj:
    
    ###########################################################################
    # init class with the mongo host, db name and pancake, pancake maker and spatula blade coll names
    def __init__(self, host_name, db_name, pm_coll_name, b_coll_name):
        self.conn = MongoClient(host_name, 27017)
        self.db = self.conn[db_name]
        self.pm_coll = self.db[pm_coll_name]  
        self.b_coll = self.db[b_coll_name]    
     
     
  
    ###########################################################################
    # get the center pose of the pancake maker surface
    def get_pancake_maker_pose(self):
        # query for all the results (there should be only one document)
        cursor = self.pm_coll.find()
            
        # get the result first(only) value
        res = cursor.next()
        
        # return pose (POS XYZ, ROT XYZ, timestamp)
        return np.array([res['pos']['x'], res['pos']['y'], res['pos']['z'],
                res['rot']['x'], res['rot']['y'], res['rot']['z'],
                res['timestamp']])
                
                
                
    ###########################################################################
    # get the center pose of the pancake maker surface
    def get_bottle_traj(self):
        # query for all the results (there should be only one document)
        cursor = self.b_coll.find()
            
        # init trajectory as numpy array
        traj = np.zeros((cursor.count(),7))   
        
        # numpy array iter
        i = 0        
        
        # iterate through results and add them to the trajectory
        for c in cursor:
            # add value to numpy array
            traj[i] = [c['pos']['x'], c['pos']['y'], c['pos']['z'],
                         c['rot']['x'], c['rot']['y'], c['rot']['z'],
                         c['timestamp']]
            # inc i
            i += 1
            
        # return traj in the form [[POS XYZ, ROT XYZ, timestamp], .. ]
        return traj   
              

              
### Usage examples
if __name__ == '__main__':
    
    # connect to the db and the collections
    f1 = RobotFlipTraj('localhost', 'robot_flip_T', 'pancake_1', 'pancake_maker_1', 'r_spatula_blade_1')    
    
    # POS xyz, ROT xyz, timestamp
    arr_pancake = f1.get_pancake_pose()      
    arr_pancake_maker = f1.get_pancake_maker_pose()    
    arr_spatula_traj = f1.get_spatula_traj()

    # connect to the db, add collections
    p1 = RobotPourTraj('localhost', 'robot_pour_T', 'pancake_maker_1', 'pancake_bottle_1')
    
    # POS xyz, ROT xyz, timestamp
    arr_pouring_pancake_maker = p1.get_pancake_maker_pose()
    arr_bottle_traj = p1.get_bottle_traj()   
    
    
    
    ###########################################################################      
    ### FIG 1, flipping
    fig1 = plt.figure(figsize=(20,20))
    
    ### SUBPLOT 1, positions
    sb1 = fig1.add_subplot(221, projection='3d')    
    # plot position of the pancake
    sb1.plot([arr_pancake[0]], [arr_pancake[1]], [arr_pancake[2]], '^',\
        markersize=10, color='blue', alpha=0.8, label='pancake center')    
    # plot position of the pancake maker
    sb1.plot([arr_pancake_maker[0]], [arr_pancake_maker[1]], [arr_pancake_maker[2]], '^',\
        markersize=10, color='red', alpha=0.8, label='pancake maker center')    
    # plot positions of the spatula
    sb1.plot(arr_spatula_traj[:,0], arr_spatula_traj[:,1], arr_spatula_traj[:,2], 'o',\
        markersize=7, color='orange', alpha=0.5, label='spatula head traj')    
    # label
    plt.title('Spatula head 3D position trajectory')
    plt.legend(loc=3,prop={'size':8})
    
    ### SUBPLOT 2, ROT X axis
    sb2 = fig1.add_subplot(222)    
    # ROT on X 
    sb2.plot(arr_spatula_traj[:,6], arr_spatula_traj[:,3],\
             'o', markersize=7,color='red', alpha=0.5)
    # labels
    plt.xlabel('time (ms)')
    plt.ylabel('x_rot')
    plt.title('Spatula head ROT on X axis')
    
        
    ### SUBPLOT 3, ROT Y axis
    sb3 = fig1.add_subplot(223)    
    # ROT on Y 
    sb3.plot(arr_spatula_traj[:,6], arr_spatula_traj[:,4],\
             'o', markersize=7,color='green', alpha=0.5)
    # labels    
    plt.xlabel('time (ms)')
    plt.ylabel('y_rot')
    plt.title('Spatula head ROT on Y axis')
    
    
    ### SUBPLOT 4, ROT Z axis
    sb4 = fig1.add_subplot(224)    
    # ROT on Z 
    sb4.plot(arr_spatula_traj[:,6], arr_spatula_traj[:,5],\
             'o', markersize=7,color='blue', alpha=0.5)
    # labels  
    plt.xlabel('time (ms)')
    plt.ylabel('z_rot')
    plt.title('Spatula head ROT on Z axis')
    
    
    
    ###########################################################################      
    ### FIG 2, pouring
    fig2 = plt.figure(figsize=(20,20))
    
    ### SUBPLOT 1, positions
    sb1 = fig2.add_subplot(221, projection='3d')   
    # plot position of the pancake maker
    sb1.plot([arr_pouring_pancake_maker[0]], [arr_pouring_pancake_maker[1]], [arr_pouring_pancake_maker[2]], '^',\
        markersize=10, color='red', alpha=0.8, label='pancake maker center')    
    # plot positions of the spatula
    sb1.plot(arr_bottle_traj[:,0], arr_bottle_traj[:,1], arr_bottle_traj[:,2], 'o',\
        markersize=7, color='orange', alpha=0.5, label='bottle top traj')    
    # label
    plt.title('Bottle top 3D position trajectory')
    plt.legend(loc=3,prop={'size':8})
    
    ### SUBPLOT 2, ROT X axis
    sb2 = fig2.add_subplot(222)    
    # ROT on X 
    sb2.plot(arr_bottle_traj[:,6], arr_bottle_traj[:,3],\
             'o', markersize=7,color='red', alpha=0.5)
    # labels
    plt.xlabel('time (ms)')
    plt.ylabel('x_rot')
    plt.title('Bottle top ROT on X axis')
    
        
    ### SUBPLOT 3, ROT Y axis
    sb3 = fig2.add_subplot(223)    
    # ROT on Y 
    sb3.plot(arr_bottle_traj[:,6], arr_bottle_traj[:,4],\
             'o', markersize=7,color='green', alpha=0.5)
    # labels    
    plt.xlabel('time (ms)')
    plt.ylabel('y_rot')
    plt.title('Bottle top ROT on Y axis')
    
    
    ### SUBPLOT 4, ROT Z axis
    sb4 = fig2.add_subplot(224)    
    # ROT on Z 
    sb4.plot(arr_bottle_traj[:,6], arr_bottle_traj[:,5],\
             'o', markersize=7,color='blue', alpha=0.5)
    # labels  
    plt.xlabel('time (ms)')
    plt.ylabel('z_rot')
    plt.title('Bottle top ROT on Z axis')
    
    
    
    
    
    

    
